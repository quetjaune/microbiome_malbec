Bioinformatic guide for metatranscriptomic analysis of *Vitis vinifera* cv. Malbec
================================================================================


Quality filtering thorugh trimmomatic
-------------------------------------
```
for f in $(ls *fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); do java -jar /usr/src/Trimmomatic-0.38/trimmomatic-0.38.jar PE -threads 8 -basein ${f}_1.fastq.gz -baseout ./1_quality_check/trimmomatic_out/${f}.fastq.gz ILLUMINACLIP:/usr/src/Trimmomatic-0.38/adapters/TruSeq3-PE-2.fa:2:30:10 SLIDINGWINDOW:4:15 LEADING:5 TRAILING:5 MINLEN:25; done
```
Removing grapevine (host) reads
--------------------------------
Download the required genome files from ENSEMBL FTP: [assembly.fasta](ftp://ftp.ensemblgenomes.org/pub/release-41/plants/fasta/vitis_vinifera/dna/) and [annotation.gtf](ftp://ftp.ensemblgenomes.org/pub/release-41/plants/gtf/vitis_vinifera)
```
wget ftp://ftp.ensemblgenomes.org/pub/release-41/plants/gtf/vitis_vinifera/Vitis_vinifera.12X.41.chr.gtf.gz
wget ftp://ftp.ensemblgenomes.org/pub/release-41/plants/fasta/vitis_vinifera/dna/Vitis_vinifera.12X.dna.toplevel.fa.gz
```
Uncompress
```
gunzip *.gz
```
Prepare the index
```
STAR --runThreadN 48 --runMode genomeGenerate --genomeDir ./ --genomeFastaFiles Vitis_vinifera.12X.dna.toplevel.fa --sjdbGTFfile Vitis_vinifera.12X.41.chr.gtf
```
Run mapping to grapevine genome
```
for f in $(ls *[1-2].fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); 
do STAR --readFilesCommand zcat --runThreadN 20 --genomeDir ../genome_star_indexed/ --readFilesIn ${f}_1.fastq.gz ${f}_2.fastq.gz --outSAMtype BAM Unsorted --quantMode TranscriptomeSAM GeneCounts --outFileNamePrefix ${f} --outReadsUnmapped Fastx;
done
```
Organelle and plant ribosomal DNA remotion 
-------------------------------------------------------------------------------------------------------------------------------------------------
Using the most representative sequence obtained in previous sortmeRNA output (rDNA from A. thaliana) as query in NCBI blast, the secuences of the best 100 target hits were downloaded, then edited to remove Ns and concatenated with organelle-derived sequences through:

```
cat rRNA_ncbi.fasta all_organelle_ADN_ARN.fasta > rRNA_organelle.fasta
```
Remove the mapped reads
```
STARWD="../raw_grpvne_reads/trimmomatic_out/"
for f in $(ls $STARWD*Unmapped.out.mate[1-2] | cut -f4 -d"/" | sed 's/Unmapped.out.mate[1-2]//' | uniq); do bowtie2 -p 30 -x rRNA_organelle -1 $STARWD${f}Unmapped.out.mate1 -2 $STARWD${f}Unmapped.out.mate2 | samtools view -bS -f 4 - | sam
tools fastq - > ${f}_rrna_org_unmap.fq ; done
```
Microbiome characterization using KrakenUniq
---------------------------------------------

Download the indexed database containing all the NCBI genomes from microbes (archeas, bacteria, virus and fungi) in NT DB (around 240 GB, is absolutely required HPC)
```
wget -c ftp://ftp.ccb.jhu.edu/pub/software/krakenuniq/Databases/nt/*
```
Prepare the file to be used as reference
```
krakenuniq-build --db microb_nt_db --kmer-len 31 --threads 10 --taxids-for-genomes --taxids-for-sequences --jellyfish-hash-size 6400M
```
Run KrakenUniq
```
krakenuniq --db microb_nt_db --fastq-input ../bowtie2_out/c22_rrna_org_unmap.fq --threads 12 --report-file REPORTFILE.tsv > READCLASSIFICATION.tsv
```
Exploring the results of KrakenUniq through [Pavian](https://github.com/fbreitwieser/pavian) in R and load the directory containing the REPORTFILE.tsv for all the samples:
```
if (!require(remotes)) { install.packages("remotes") }
remotes::install_github("fbreitwieser/pavian")
    pavian::runApp(port=5000)
  
  shiny::runGitHub("fbreitwieser/pavian", subdir = "inst/shinyapp")
```
